// tslint:disable
// this is an auto generated file. This will be overwritten

export const onCreateEntry = `subscription OnCreateEntry {
  onCreateEntry {
    id
    date
    grateful
    greatDay
    iAm
    amazingThings
    better
    notes
  }
}
`;
