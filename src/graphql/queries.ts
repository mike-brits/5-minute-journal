// tslint:disable
// this is an auto generated file. This will be overwritten

export const getEntry = `query GetEntry($userId: ID, $date: String!) {
  getEntry(userId: $userId, date: $date) {
    id
    date
    grateful
    greatDay
    iAm
    amazingThings
    better
    notes
  }
}
`;
export const listEntries = `query ListEntries(
  $filter: TableEntryFilterInput
  $limit: Int
  $nextToken: String
) {
  listEntries(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      date
      grateful
      greatDay
      iAm
      amazingThings
      better
      notes
    }
    nextToken
  }
}
`;
export const getAllEntries = `query GetAllEntries($userId: ID) {
  getAllEntries(userId: $userId) {
    id
    date
    grateful
    greatDay
    iAm
    amazingThings
    better
    notes
  }
}
`;
export const getStreak = `query GetStreak($userId: ID, $date: String) {
  getStreak(userId: $userId, date: $date) {
    date
    count
  }
}
`;
