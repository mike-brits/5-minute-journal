// tslint:disable
// this is an auto generated file. This will be overwritten

export const createEntry = `mutation CreateEntry($input: CreateEntryInput!) {
  createEntry(input: $input) {
    id
    date
    grateful
    greatDay
    iAm
    amazingThings
    better
    notes
  }
}
`;
export const updateEntry = `mutation UpdateEntry($input: UpdateEntryInput!) {
  updateEntry(input: $input) {
    id
    date
    grateful
    greatDay
    iAm
    amazingThings
    better
    notes
  }
}
`;
export const deleteEntry = `mutation DeleteEntry($input: DeleteEntryInput!) {
  deleteEntry(input: $input) {
    id
    date
    grateful
    greatDay
    iAm
    amazingThings
    better
    notes
  }
}
`;
