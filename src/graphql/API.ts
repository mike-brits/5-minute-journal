/* tslint:disable */
//  This file was automatically generated and should not be edited.

export type CreateEntryInput = {
  grateful?: Array< string | null >,
  greatDay?: Array< string | null >,
  iAm?: Array< string | null >,
  amazingThings?: Array< string | null >,
  better?: string,
  notes?: string | null,
  userId?: string | null,
};

export type UpdateEntryInput = {
  id?: string | null,
  date: string,
  grateful?: Array< string | null > | null,
  greatDay?: Array< string | null > | null,
  iAm?: Array< string | null > | null,
  amazingThings?: Array< string | null > | null,
  better?: string | null,
  notes?: string | null,
  userId?: string | null,
};

export type DeleteEntryInput = {
  date: string,
  userId?: string | null,
};

export type TableEntryFilterInput = {
  id?: TableIDFilterInput | null,
  date?: TableStringFilterInput | null,
  better?: TableStringFilterInput | null,
  notes?: TableStringFilterInput | null,
};

export type TableIDFilterInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
};

export type TableStringFilterInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
};

export type CreateEntryMutationVariables = {
  input: CreateEntryInput,
};

export type CreateEntryMutation = {
  createEntry:  {
    __typename: "Entry",
    id: string | null,
    date: string,
    grateful: Array< string | null > | null,
    greatDay: Array< string | null > | null,
    iAm: Array< string | null > | null,
    amazingThings: Array< string | null > | null,
    better: string | null,
    notes: string | null,
  } | null,
};

export type UpdateEntryMutationVariables = {
  input: UpdateEntryInput,
};

export type UpdateEntryMutation = {
  updateEntry:  {
    __typename: "Entry",
    id: string | null,
    date: string,
    grateful: Array< string | null > | null,
    greatDay: Array< string | null > | null,
    iAm: Array< string | null > | null,
    amazingThings: Array< string | null > | null,
    better: string | null,
    notes: string | null,
  } | null,
};

export type DeleteEntryMutationVariables = {
  input: DeleteEntryInput,
};

export type DeleteEntryMutation = {
  deleteEntry:  {
    __typename: "Entry",
    id: string | null,
    date: string,
    grateful: Array< string | null > | null,
    greatDay: Array< string | null > | null,
    iAm: Array< string | null > | null,
    amazingThings: Array< string | null > | null,
    better: string | null,
    notes: string | null,
  } | null,
};

export type GetEntryQueryVariables = {
  userId?: string | null,
  date: string,
};

export type GetEntryQuery = {
  getEntry:  {
    __typename: "Entry",
    id: string | null,
    date: string,
    grateful: Array< string | null > | null,
    greatDay: Array< string | null > | null,
    iAm: Array< string | null > | null,
    amazingThings: Array< string | null > | null,
    better: string | null,
    notes: string | null,
  } | null,
};

export type ListEntriesQueryVariables = {
  filter?: TableEntryFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListEntriesQuery = {
  listEntries:  {
    __typename: "EntryConnection",
    items:  Array< {
      __typename: "Entry",
      id: string | null,
      date: string,
      grateful: Array< string | null > | null,
      greatDay: Array< string | null > | null,
      iAm: Array< string | null > | null,
      amazingThings: Array< string | null > | null,
      better: string | null,
      notes: string | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetAllEntriesQueryVariables = {
  userId?: string | null,
};

export type GetAllEntriesQuery = {
  getAllEntries:  Array< {
    __typename: "Entry",
    id: string | null,
    date: string,
    grateful: Array< string | null > | null,
    greatDay: Array< string | null > | null,
    iAm: Array< string | null > | null,
    amazingThings: Array< string | null > | null,
    better: string | null,
    notes: string | null,
  } | null > | null,
};

export type GetStreakQueryVariables = {
  userId?: string | null,
  date?: string | null,
};

export type GetStreakQuery = {
  getStreak:  {
    __typename: "Streak",
    date: string,
    count: number,
  } | null,
};

export type OnCreateEntrySubscription = {
  onCreateEntry:  {
    __typename: "Entry",
    id: string | null,
    date: string,
    grateful: Array< string | null > | null,
    greatDay: Array< string | null > | null,
    iAm: Array< string | null > | null,
    amazingThings: Array< string | null > | null,
    better: string | null,
    notes: string | null,
  } | null,
};
