import dev from './dev-api-credentials';
import prod from './production-api-credentials';

export default (type : string) => {
    switch(type){
        case 'production':
            return prod;
        case 'dev':
        default:
            return dev;
    }
}
