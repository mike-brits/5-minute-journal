import React, { Component } from 'react';
import Entry from './components/Entry';
import EntryList from './components/EntryList';
import EntryEdit from './components/EntryEdit';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Amplify, {Auth} from 'aws-amplify';
import { withAuthenticator } from 'aws-amplify-react';
import './App.css';
import { initializeIcons } from './utils/icons';
import getCredentials from './config/getCredentials';

const config = getCredentials(process.env.NODE_ENV);
Amplify.configure(config);

const getJwt = async () => {
    const session  = await Auth.currentSession();
    console.log(session);
}

initializeIcons();

class App extends Component {
    render() {
        getJwt();
        return (
            <div className="App">
                <Router>
                    <Switch>
                        <Route path="/" exact component={EntryList} />
                        <Route path="/new" exact component={EntryEdit} />
                        <Route path="/:id" exact component={Entry} />
                        <Route
                            path="/:id/edit"
                            exact
                            render={({ match }) => <EntryEdit edit match={match} />}
                        />
                    </Switch>
                </Router>
            </div>
        );
    }
}

export default withAuthenticator(App);
