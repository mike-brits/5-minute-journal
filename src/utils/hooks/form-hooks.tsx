import {
    ChangeEvent,
    ChangeEventHandler,
    FocusEventHandler,
    useMemo,
    useRef,
    useState,
} from 'react';
import React from 'react';

export const useValidator = (f: Array<ValidatorField>): boolean => {
    const fields = f.map(e => {
        //e.validate();
        return e.isValid;
    });
    return useMemo(() => {
        return fields.every(x => x);
    }, fields);
};

export const isEmail = () => (email: string): boolean => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

export const hasLength = (len: number) => (value: string): boolean => {
    return value.length >= len;
};

export const mustEqualField = (field: ValidatorField) => (value: string | number): boolean => {
    return field.props.value === value;
};

export function useValidatorField(opts: ValidatorFieldOptions): ValidatorField {
    const options = {
        value: '',
        required: false,
        ...opts,
    };
    const validators = Array.prototype.slice.call(arguments, 1);

    const checkValidation = (): boolean => {
        if (value) return validators.length ? validators.every(v => v(value)) : true;
        return !options.required;
    };

    const [value, setValue] = useState(options.value);
    const [isValid, setValid] = useState(checkValidation());
    const [isEdited, setEdited] = useState(!!options.value);
    const [isDisabled, setDisabled] = useState(false);
    const ref = useRef(null);

    const validate = () => {
        setValid(checkValidation());
    };

    const forceValidate = () => {
        validate();
        setEdited(true);
    };

    const handleBlur = (): void => {
        setEdited(true);
        validate();
    };

    const handleChange = (e: any) => {
        setValue(e.target.value);
        validate();
    };

    const handleDisabled = (disabled = true) => {
        setDisabled(disabled);
    };

    const clearField = () => {
        setValue('');
        setEdited(false);
        setValid(!options.required);
    };

    return {
        props: {
            //ref,
            value,
            onChange: handleChange,
            onBlur: handleBlur,
            disabled: isDisabled,
        },
        clearField,
        validate,
        forceValidate,
        isValid,
        isEdited: !!value,
        shouldError: !isValid && isEdited,
        disable: handleDisabled,
        value,
    };
}

interface ValidatorFieldOptions {
    required?: boolean;
    value?: any;
}

interface ValidatorField {
    props: ValidatorFieldProps;
    validate: Function;
    forceValidate: Function;
    isValid: boolean;
    isEdited: boolean;
    shouldError: boolean;
    disable: Function;
    clearField: Function;
    value: any;
}

interface ValidatorFieldProps {
    //ref: any;
    value: any;
    onChange: ChangeEventHandler;
    onBlur: FocusEventHandler;
    disabled: boolean;
}
