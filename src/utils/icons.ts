import { library } from '@fortawesome/fontawesome-svg-core'
import { faArrowLeft, faSun, faMoon } from '@fortawesome/pro-solid-svg-icons';

export const initializeIcons = () => {
    library.add(faArrowLeft, faSun, faMoon);
};
