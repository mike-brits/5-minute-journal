import {Link} from "react-router-dom";
import React from 'react';
import styled from "styled-components";

export const BackButton = styled(Link)`
  color: #264F80;
  margin-right: 16px;
`;

export const Button = styled.button`
  background: transparent;
  border: 1px solid #264F80;
  color: #264F80;
  font-family: 'Aleo', serif;
  font-size: 16px;
  padding: 8px 24px;
  cursor: pointer;
  text-decoration: none;
`;

export const LinkButton = (props : any) => <Button as={Link} {...props} /> ;