import styled from 'styled-components';
import React from 'react';
import {Link} from "react-router-dom";

export const Heading = styled.h1`
  font-size: 20px;
  font-weight: bold;
  color: #264F80;
  margin: 0;
`;

export const Body = styled.div``;

export const BodyCenter = styled.div`
  text-align: center;
`;

export const Bold = styled.span`
  font-weight: bold;
`;


export const Small = styled.small`
  font-size: 12px;
  color: #9B9B9B;
  text-decoration: none;
`;

export const SmallLink = (props) => <Small as={Link} {...props} />;

export const Input = styled.input`
  background: transparent;
  border-bottom: 1px solid #C5C5C5;
  width: 200px;
`;

export const ListItem = styled.li`
  margin-left: 0;
  margin-bottom: 8px;
`;
