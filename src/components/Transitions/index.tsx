import React, {ReactNode} from 'react';
import { Trail } from 'react-spring';

interface SlideInProps {
    children : Array<ReactNode>
}

export const SlideIn = ({children} : SlideInProps) => {
    return (
        <Trail
            items={children}
            keys={(item: any) => item.key}
            from={{ transform: 'translate3d(0,40px,0)', opacity: 0 }}
            to={{ transform: 'translate3d(0,0px,0)', opacity: 1 }}
        >
            {item => props => <div style={props}>{item}</div>}
        </Trail>
    );
};
