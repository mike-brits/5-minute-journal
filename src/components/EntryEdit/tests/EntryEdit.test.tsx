import React from 'react';
import ReactDOM from 'react-dom';
import EntryEdit from '../EntryEdit';

describe('EntryEdit', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<EntryEdit />, div);
        ReactDOM.unmountComponentAtNode(div);
    });
})
