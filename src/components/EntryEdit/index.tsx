import React from 'react';
import EntryEdit from './EntryEdit';
import { EntryType } from '../Entry/Entry';
import { API, graphqlOperation, Auth } from 'aws-amplify';
import moment from 'moment';
import * as mutations from '../../graphql/mutations';
import uuid from 'uuid';
import { useEntry } from '../Entry';

const createEntry = async (fields: EntryType) => {
    const input = {
        ...fields
    };
    await API.graphql(graphqlOperation(mutations.createEntry, { input }));
    // await API.graphql(graphqlOperation(mutations.createStreak, {
    //         input: {
    //             userId: user.username,
    //             date: 'streak|' + moment().format('YYYY-MM-DD'),
    //             count: 0,
    //         },
    //     }));
};

const editEntry = async (fields: EntryType) => {
    const input = {
        ...fields,
    };
    console.log('input', input);
    return await API.graphql(graphqlOperation(mutations.updateEntry, { input }));
};

interface EntryEditProps {
    edit?: boolean;
    match?: any;
}

export default ({ edit, match }: EntryEditProps) => {
    if (!edit) return <EntryEdit onSubmit={(fields: EntryType) => createEntry(fields)} />;

    const [loaded, error, entry] = useEntry(match.params.id);
    if (error) return <div>Error: {error}</div>;
    if (!loaded) return <div>Loading...</div>;

    return <EntryEdit edit entry={entry} onSubmit={(fields: EntryType) => editEntry(fields)} />;
};
