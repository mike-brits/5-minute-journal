import styled from 'styled-components';
import React from 'react';

export const Container = styled.div`
    display: block;
`;

export const Error = styled.div`
    color: #ba0000;
    font-size: 12px;
    margin-top: 4px;
    margin-left: 16px;
`;

export const Entry = styled.div`
    padding-bottom: 32px;
`;

export const Field = styled.div`
    margin: 4px 0;
    display: flex;
    align-items: center;
`;

const TextArea = (props: any) => {
    return (
        <textarea
            {...props}
            cols={42}
            ref={null}
            rows={props.value ? Math.ceil(props.value.length / 40) : 1}
        />
    );
};

export const Input = styled(TextArea)`
    border: 0;
    border-bottom: 1px solid #c5c5c5;
    width: 300px;
    background: transparent;
    font-family: 'Aleo', serif;
    font-size: 16px;
    padding: 6px 4px 4px 4px;
    margin-left: 5px;
    resize: none;
    max-width: 80%;
    &:focus {
        outline: 0;
    }
`;
