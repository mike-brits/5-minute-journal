import React, { useState } from 'react';
import { Container, ContentBlock, ContentWrapper, ContentContainer } from '../Entry/Entry.styles';
import { Input, Entry, Field } from './EntryEdit.styles';
import { useValidatorField } from '../../utils/hooks/form-hooks';
import { Error } from './EntryEdit.styles';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { Heading } from '../StyledComponents/text';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { BackButton, Button } from '../StyledComponents/Buttons';
import { SlideIn } from '../Transitions';
import { EntryType } from '../Entry/Entry';

const EntryEdit = ({ onSubmit, entry, edit }: Props) => {
    const [error, setError] = useState('');
    const [complete, setComplete] = useState(false);
    const defaultValue = '';

    const getValue = (value: any) => {
        return value || defaultValue;
    };

    let grateful: any;
    const [gratefulOne, gratefulTwo, gratefulThree] = (grateful = [
        useValidatorField({
            required: false,
            value: getValue(entry && entry.grateful[0]),
        }),
        useValidatorField({
            required: false,
            value: getValue(entry && entry.grateful[1]),
        }),
        useValidatorField({
            required: false,
            value: getValue(entry && entry.grateful[2]),
        }),
    ]);

    let greatDay: any;
    const [greatDayOne, greatDayTwo, greatDayThree] = (greatDay = [
        useValidatorField({
            required: false,
            value: getValue(entry && entry.greatDay[0]),
        }),
        useValidatorField({
            required: false,
            value: getValue(entry && entry.greatDay[1]),
        }),
        useValidatorField({
            required: false,
            value: getValue(entry && entry.greatDay[2]),
        }),
    ]);

    let iAm: any;
    const [iAmOne, iAmTwo, iAmThree] = (iAm = [
        useValidatorField({
            required: false,
            value: getValue(entry && entry.iAm[0]),
        }),
        useValidatorField({
            required: false,
            value: getValue(entry && entry.iAm[1]),
        }),
        useValidatorField({
            required: false,
            value: getValue(entry && entry.iAm[2]),
        }),
    ]);

    let amazingThings: any;
    const [amazingThingsOne, amazingThingsTwo, amazingThingsThree] = (amazingThings = [
        useValidatorField({
            required: false,
            value: getValue(entry && entry.amazingThings[0]),
        }),
        useValidatorField({
            required: false,
            value: getValue(entry && entry.amazingThings[1]),
        }),
        useValidatorField({
            required: false,
            value: getValue(entry && entry.amazingThings[2]),
        }),
    ]);

    const betterDay = useValidatorField({
        required: false,
        value: getValue(entry && entry.better),
    });
    const notes = useValidatorField({
        required: false,
        value: getValue(entry && entry.notes),
    });

    const fields = [...grateful, ...greatDay, ...iAm, ...amazingThings, betterDay, notes];

    const handleSubmit = async () => {
        let allValid = true;
        fields.forEach(item => {
            item.forceValidate();
            if (!item.isValid) {
                console.log('invalid', item.value);
                allValid = false;
            }
        });
        console.log('hello', allValid);
        if (!allValid) return;
        try {
            const values = {
                grateful: grateful.map((i: any) => i.value),
                greatDay: greatDay.map((i: any) => i.value),
                iAm: iAm.map((i: any) => i.value),
                amazingThings: amazingThings.map((i: any) => i.value),
                better: betterDay.value,
                notes: notes.value,
                date: entry ? entry.date : undefined,
                id: entry ? entry.id : undefined,
            };

            await onSubmit(values);
            fields.forEach(field => {
                field.clearField();
            });
            setComplete(true);
        } catch (e) {
            console.error(e);
            setError(e.errors[0].message);
        }
    };

    if (complete)
        return (
            <div>
                <h3>Well done, keep going!</h3>
                <Link to={'/'}> Back Home </Link>
            </div>
        );

    return (
        <Container>
            {error && <h3>{error}</h3>}
            <Heading>
                <BackButton to="/">
                    <FontAwesomeIcon icon="arrow-left" />
                </BackButton>
                {moment().format('dddd Do MMMM YYYY')}
            </Heading>
            <ContentWrapper>
                <ContentContainer>
                    <ContentBlock>
                        <SlideIn>
                            <Entry key={1}>
                                3 Reasons to be grateful:
                                <Field>
                                    1. <Input {...gratefulOne.props} />
                                    {gratefulOne.shouldError && (
                                        <Error>This field is required</Error>
                                    )}
                                </Field>
                                <Field>
                                    2. <Input {...gratefulTwo.props} />
                                    {gratefulTwo.shouldError && (
                                        <Error>This field is required</Error>
                                    )}
                                </Field>
                                <Field>
                                    3. <Input {...gratefulThree.props} />
                                    {gratefulThree.shouldError && (
                                        <Error>This field is required</Error>
                                    )}
                                </Field>
                            </Entry>
                            <Entry key={2}>
                                3 Things that would make today great:
                                <Field>
                                    1. <Input {...greatDayOne.props} />
                                    {greatDayOne.shouldError && (
                                        <Error>This field is required</Error>
                                    )}
                                </Field>
                                <Field>
                                    2. <Input {...greatDayTwo.props} />
                                    {greatDayTwo.shouldError && (
                                        <Error>This field is required</Error>
                                    )}
                                </Field>
                                <Field>
                                    3. <Input {...greatDayThree.props} />
                                    {greatDayThree.shouldError && (
                                        <Error>This field is required</Error>
                                    )}
                                </Field>
                            </Entry>
                            <Entry key={3}>
                                Daily Affirmation. I am:
                                <Field>
                                    1. <Input {...iAmOne.props} />
                                    {iAmOne.shouldError && <Error>This field is required</Error>}
                                </Field>
                                <Field>
                                    2. <Input {...iAmTwo.props} />
                                    {iAmTwo.shouldError && <Error>This field is required</Error>}
                                </Field>
                                <Field>
                                    3. <Input {...iAmThree.props} />
                                    {iAmThree.shouldError && <Error>This field is required</Error>}
                                </Field>
                            </Entry>
                        </SlideIn>
                    </ContentBlock>
                    <ContentBlock>
                        <SlideIn>
                            <Entry key={1}>
                                3 amazing things that happened today:
                                <Field>
                                    1. <Input {...amazingThingsOne.props} />
                                    {amazingThingsOne.shouldError && (
                                        <Error>This field is required</Error>
                                    )}
                                </Field>
                                <Field>
                                    2. <Input {...amazingThingsTwo.props} />
                                    {amazingThingsTwo.shouldError && (
                                        <Error>This field is required</Error>
                                    )}
                                </Field>
                                <Field>
                                    3. <Input {...amazingThingsThree.props} />
                                    {amazingThingsThree.shouldError && (
                                        <Error>This field is required</Error>
                                    )}
                                </Field>
                            </Entry>
                            <Entry key={2}>
                                What would have made today better?
                                <Field>
                                    1. <Input {...betterDay.props} />
                                    {betterDay.shouldError && <Error>This field is required</Error>}
                                </Field>
                            </Entry>
                            <Entry key={3}>
                                Notes
                                <Field>
                                    1. <Input {...notes.props} />
                                </Field>
                            </Entry>
                            <Button key={4} onClick={handleSubmit}>
                                {edit ? 'Save Changes' : 'Submit'}
                            </Button>
                        </SlideIn>
                    </ContentBlock>
                </ContentContainer>
            </ContentWrapper>
        </Container>
    );
};

interface Props {
    onSubmit: Function;
    entry?: EntryType;
    edit?: boolean;
}

export default EntryEdit;
