import React from 'react';
import { Container, ContentBlock, ContentContainer, ContentWrapper } from './Entry.styles';
import EntryField from '../EntryField';
//import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import moment from 'moment';
import {Heading, SmallLink} from '../StyledComponents/text';
import { BackButton } from '../StyledComponents/Buttons';
import { SlideIn } from '../Transitions';
import {Link} from "react-router-dom";

export interface EntryType {
    date: string;
    grateful: Array<string>;
    greatDay: Array<string>;
    iAm: Array<string>;
    amazingThings: Array<string>;
    better: string;
    notes?: string;
    id?: string
}

const Entry = ({ date, grateful, greatDay, iAm, amazingThings, better, notes }: EntryType) => {
    return (
        <Container>
            <Heading id="entry-date">
                <BackButton to="/">
                    <FontAwesomeIcon icon="arrow-left" />
                </BackButton>
                {moment(date).format('dddd Do MMMM YYYY')}
            </Heading>
            <SmallLink style={{marginLeft : 36}} to={`/${date}/edit`}>Edit Entry</SmallLink>
            <ContentWrapper>
                <ContentContainer style={{paddingLeft : 76}}>
                    <ContentBlock>
                        <SlideIn>
                            <FontAwesomeIcon
                                color="#264F80"
                                icon="sun"
                                size="3x"
                                key={1}
                                style={{ marginBottom: 36 }}
                            />
                            <EntryField
                                id="grateful"
                                items={grateful}
                                title="I am grateful for"
                                key={2}
                            />
                            <EntryField
                                key={3}
                                id="greatDay"
                                items={greatDay}
                                title="3 things that would make today great:"
                            />
                            <EntryField
                                id="iAm"
                                items={iAm}
                                title="Daily Affirmation. I am:"
                                key={4}
                            />
                        </SlideIn>
                    </ContentBlock>
                    <ContentBlock dark>
                        <SlideIn>
                            <FontAwesomeIcon
                                icon="moon"
                                color="#264F80"
                                size="3x"
                                key={1}
                                style={{ marginBottom: 36 }}
                            />
                            <EntryField
                                id="amazingThings"
                                items={amazingThings}
                                title="3 amazing things that happened today"
                                key={2}
                            />
                            <EntryField
                                id="better"
                                item={better}
                                title="Things that would have made today better:"
                                key={3}
                            />
                            <EntryField id="notes" item={notes} title="Notes" key={4} />
                        </SlideIn>
                    </ContentBlock>
                </ContentContainer>
            </ContentWrapper>
        </Container>
    );
};

export default Entry;
