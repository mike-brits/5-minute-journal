import styled from 'styled-components';

export const Container = styled.div`
  align-items: center;
  padding: 40px 90px;
  max-width: 1200px;
  margin: 0 auto;
  @media (max-width: 1000px) {
      padding: 16px;
  }
`;

export const ContentWrapper = styled.div`
    height: 100vh;
    display: flex;
    flex-flow: column;
    align-items: center;
    margin-top: 40px;
`;

export const ContentContainer = styled.div`
    display: flex;
    flex-flow: row;
    width: 100%;
    max-width: 1200px;
    @media (max-width: 1000px) {
      flex-flow: column;
    }
`;

interface ContentBlockProps {
    dark?: boolean
}

export const ContentBlock = styled.div`
  position: relative;
  width: 50%;
  color: ${({dark} : ContentBlockProps) => dark ? "inherit" : "inherit"};
  @media (max-width: 1000px) {
      width: 100%
    }
`;
