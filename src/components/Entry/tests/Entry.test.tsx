import React from 'react';
import ReactDOM from 'react-dom';
import Entry from '../Entry';
import { mount } from 'enzyme';

const mockData = {
    date: '11-22-63',
    grateful: ['g1', 'g2', 'g3'],
    greatDay: ['gd1', 'gd2', 'gd3'],
    iAm: ['IAM1', 'IAM2', 'IAM3'],
    amazingThings: ['A1', 'A2', 'A3'],
    better: 'better',
    notes: 'notes',
};

describe('Entry', () => {
    const entryComponent = mount(<Entry {...mockData} />);

    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<Entry {...mockData} />, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    it('has all the expected fields', () => {
        const fields = [
            { name: 'grateful', count: 3 },
            { name: 'greatDay', count: 3 },
            { name: 'iAm', count: 3 },
            { name: 'amazingThings', count: 3 },
            { name: 'better', count: 1 },
            { name: 'notes', count: 1 },
        ];

        for (let i = 0; i < fields.length; i++) {
            expect(
                entryComponent.find(`div#${fields[i].name} #${fields[i].name}-items li`).length,
            ).toBe(fields[i].count);
        }

        // expect(entryComponent.find('div#grateful #grateful-items li').length).toBe(3);
        // expect(entryComponent.find('div#greatDay #greatDay-items li').length).toBe(3);
        // expect(entryComponent.find('div#iAm #iAm-items li').length).toBe(3);
        // expect(entryComponent.find('div#amazingThings #amazingThings-items li').length).toBe(3);
        // expect(entryComponent.find('div#better #better-items li').length).toBe(1);
        // expect(entryComponent.find('div#notes #notes-items li').length).toBe(1);
    });
});
