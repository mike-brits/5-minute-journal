import React, { useEffect, useState } from 'react';
import Entry from './Entry';
import { API, graphqlOperation } from 'aws-amplify';
import * as queries from '../../graphql/queries';
import {Auth} from 'aws-amplify';

export const getEntry = async (date: string) => {
    //await addUserToTeam('Rocket');
    const result = (await API.graphql(
        graphqlOperation(queries.getEntry, {
            date: date,
        }),
    )) as any;
    console.log('hello', result);
    return result.data.getEntry;
};

const mutation = `mutation {
    addUserToTeam(teamId: $team, username: $username)
}`;

export const addUserToTeam = async (team: string) => {
    const user = await Auth.currentAuthenticatedUser();
    const creds = await Auth.currentSession();
    console.log('user', user, creds);
    const result = (await API.graphql(
        graphqlOperation(mutation, {
            team,
            username: user.username
        }),
    )) as any;
    console.log('result', result);
    return result.data;
};



export const useEntry = (id : string) => {
    const [entry, setEntry] = useState<any>(null);
    const [error, setError] = useState<string>('');
    const [loaded, setLoaded] = useState<boolean>(false);
    useEffect(() => {

        getEntry(id)
            .then(value => {
                setEntry(value);
                setLoaded(true);
            })
            .catch(e => {
                console.error(e);
                setError('There was an error getting the data');
            });
    }, []);

    return [loaded, error, entry];
};

export default ({ match }: Props) => {
    const [loaded, error, entry] = useEntry(match.params.id);
    if (error) {
        return <div>{error}</div>;
    }
    if (!loaded) {
        return <div>Loading...</div>;
    }
    return <Entry {...entry} />;
};

interface Props {
    match: any;
}
