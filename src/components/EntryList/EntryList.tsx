import React from 'react';
import { Container, EntriesList, Entry, Divider, EntryTitle } from './EntryList.styles';
import { Link } from 'react-router-dom';
import { Heading, Body, Bold } from '../StyledComponents/text';
import { SlideIn } from '../Transitions';
import { LinkButton } from '../StyledComponents/Buttons';

export interface EntryListItem {
    id: string;
    date: string;
    notes?: string;
}

const EntryList = ({ entries, streak }: Props) => {
    return (
        <Container>
            <Heading style={{ marginBottom: 16 }}>5 Minute Journal</Heading>
            {streak ? (
                <LinkButton to={`/${streak.date}/edit`}>
                    Update Today's Entry
                </LinkButton>
            ) : (
                <LinkButton to={'/new'}>
                    New Entry
                </LinkButton>
            )}
            {entries ? (
                <EntriesList>
                    <SlideIn>
                        {entries.map(({ id, date, notes }, index) => (
                            <Entry key={index}>
                                <EntryTitle to={'/' + date}>
                                    <Bold>{date}</Bold>
                                </EntryTitle>
                                <Body style={{ maxWidth: 300, margin: '8px auto' }}>{notes}</Body>
                                <Divider />
                            </Entry>
                        ))}
                    </SlideIn>
                </EntriesList>
            ) : (
                <div>No entries </div>
            )}
        </Container>
    );
};

export interface Streak {
    date: string;
    count: number;
}

interface Props {
    entries: Array<EntryListItem>;
    streak: Streak | null;
}

export default EntryList;
