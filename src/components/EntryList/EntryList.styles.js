import styled from 'styled-components';
import {Link} from "react-router-dom";

export const Container = styled.div`
    display: block;
    text-align: center;
    margin-top: 40px;
`;

export const EntriesList = styled.ul`
    padding: 0;
    margin: 40px 0 0 0;
    list-style: none;
`;

export const Entry = styled.div`
`;

export const EntryTitle = styled(Link)`
  text-decoration: none;
  font-weight: bold;
  color: #3D77BB;
`;

export const Divider = styled.div`
    height: 24px;
    width: 1px;
    margin: 36px auto;
    border-right: 1px solid #d3d3d3;
`;
