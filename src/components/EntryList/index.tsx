import React, { useState, useEffect } from 'react';
import EntryList, { EntryListItem, Streak } from './EntryList';
import { Auth, API, graphqlOperation } from 'aws-amplify';
import * as queries from '../../graphql/queries';
import * as subscriptions from '../../graphql/subscriptions';
import moment from 'moment';

const getEntryList = async () => {
    const result = (await API.graphql(
        graphqlOperation(queries.getAllEntries),
    )) as any;
    return result.data.getAllEntries;
};

const getStreak = async () => {
    const result = (await API.graphql(
        graphqlOperation(queries.getStreak, {
            date: 'streak|' + moment().format('YYYY-MM-DD'),
        }),
    )) as any;

    return result.data.getStreak
        ? {
              ...result.data.getStreak,
              date: result.data.getStreak.date.split('|')[1],
          }
        : null;
};

const subscribeToEntries = async (next: Function) => {
    const subscription = (await API.graphql(graphqlOperation(subscriptions.onCreateEntry))) as any;

    return await subscription.subscribe({
        closed: false,
        complete(): void {
            console.log('Complete!');
        },
        error(errorValue: any): void {
            console.error(errorValue);
        },
        next(data: any): void {
            console.log('Hello!', data);
            next(data);
        },
    });
};

export default () => {
    const [entries, setEntry] = useState<Array<EntryListItem>>([]);
    const [loaded, setLoaded] = useState(false);
    const [streak, setStreak] = useState<Streak | null>(null);
    const getEntries = () => {
        getEntryList()
            .then(items => {
                console.log(items);
                setEntry(items);
                setLoaded(true);
            })
            .catch(e => {
                console.error(e);
            });
    };

    const getStreakObject = () => {
        getStreak()
            .then(streak => setStreak(streak))
            .catch(e => console.error(e));
    };

    let subscription: any;
    subscribeToEntries(() => {
        getEntries();
        console.log('Subscription', entries);
    }).then(sub => {
        subscription = sub;
    });

    useEffect(() => {
        getEntries();
        getStreakObject();
        return async () => {
            await subscription.unsubscribe();
        };
    }, []);
    if (!loaded) {
        return <div>Loading...</div>;
    }
    return <EntryList streak={streak} entries={entries} />;
};
