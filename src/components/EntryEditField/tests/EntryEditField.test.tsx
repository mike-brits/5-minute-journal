import React from 'react';
import ReactDOM from 'react-dom';
import EntryEditField from '../EntryEditField';

describe('EntryEditField', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<EntryEditField />, div);
        ReactDOM.unmountComponentAtNode(div);
    });
})
