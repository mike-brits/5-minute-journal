import React, {useState} from 'react';
import { Container } from './EntryEditField.styles';

const EntryEditField = ({ input, onChange }: Props) => {

    return <Container>EntryEditField</Container>;
};

interface Props {
    input: Array<string>;
    onChange: Function;
}

export default EntryEditField;
