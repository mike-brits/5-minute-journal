import React from 'react';
import {Field, Items, Title} from './EntryField.styles';
import {ListItem} from "../StyledComponents/text";

const EntryField = ({ id, items, item, title }: Props) => {
    return (
        <Field id={id}>
            <Title id={id + '-title'}>{title}</Title>
            <Items id={id + '-items'}>
                {items ? (
                    items.map((item, index) => (
                        <ListItem key={index}>
                            {item}
                        </ListItem>
                    ))
                ) : (
                    <ListItem>{item}</ListItem>
                )}
            </Items>
        </Field>
    );
};

interface Props {
    id: string;
    items?: Array<string>;
    title: string;
    item?: string;
}

export default EntryField;
