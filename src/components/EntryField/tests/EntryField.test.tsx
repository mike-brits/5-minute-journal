import React from 'react';
import ReactDOM from 'react-dom';
import EntryField from '../EntryField';

describe('EntryField', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<EntryField />, div);
        ReactDOM.unmountComponentAtNode(div);
    });
})
