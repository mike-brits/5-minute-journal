import styled from 'styled-components';

export const Field = styled.div`
  margin-bottom: 20px;
`;

export const Title = styled.div`
    font-weight: bold;
    margin-bottom: 12px;
`;

export const Items = styled.ol`
    padding-left: 14px;
    margin: 0;
`;

export const Container = styled.div`
    display: block;
`;
